package main

fun main() {
    val simulation = Simulation()
    // ...loading files
    val itemsPhase1 = simulation.loadItems("C:/Users/Meme/Desktop/Android Trainee/Week 1/phase-1.txt")
    val itemsPhase2 = simulation.loadItems("C:/Users/Meme/Desktop/Android Trainee/Week 1/phase-2.txt")

    // ...simulation for rockets U1
    val rocketsU1Phase1 = simulation.loadU1(itemsPhase1)
    val rocketsU1Phase2 = simulation.loadU1(itemsPhase2)
    print("\nROCKET U1, PHASE 1: ")
    simulation.runSimulation(rocketsU1Phase1)
    print("\nROCKET U1, PHASE 2: ")
    simulation.runSimulation(rocketsU1Phase2)

    // ...simulation for rockets U2
    val rocketsU2Phase1 = simulation.loadU2(itemsPhase1)
    val rocketsU2Phase2 = simulation.loadU2(itemsPhase2)
    print("\nROCKET U2, PHASE 1: ")
    simulation.runSimulation(rocketsU2Phase1)
    print("\nROCKET U2, PHASE 2: ")
    simulation.runSimulation(rocketsU2Phase2)

}