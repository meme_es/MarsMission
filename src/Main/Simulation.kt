package main

import spaceships.Rocket
import spaceships.U1
import spaceships.U2
import java.io.File

class Simulation {

    fun runSimulation(rockets: ArrayList<Rocket>) {
        var crashes = 0
        var successes = 0
        var budget = 0.0

        println("Starting simulation...")

        for (rocket in rockets) {
            var ended = false
            while (!ended) {
                if (rocket.launch() && rocket.land()) {
                    ended = true
                    successes++
                }
                else {
                    crashes++
                }

                budget += rocket.cost
            }
        }

        println("Total budget: $budget, ")
        println("Total launches: ${successes + crashes}, succeeded: $successes, fails: $crashes")

        println("Simulation Ended, have a nice day!")
    }

    fun loadItems(fileName: String): ArrayList<Item> {
        val items = ArrayList<Item>()

        File(fileName).forEachLine {
            val item = it.split("=")
            items.add(Item(item[0].trim(), item[1].trim().toInt()))
        }

        val tmpItems = items.sortedWith(compareByDescending { it.weight })

        return ArrayList(tmpItems)
    }

    fun loadU1(items: ArrayList<Item>): ArrayList<Rocket> {
        val rockets = ArrayList<Rocket>()

        items.forEach {
            var stored = false
            for (rocket in rockets) {
                if(rocket.canCarry(it)) {
                    rocket.carry(it)
                    stored = true
                    break
                }
            }
            
            if (!stored) {
                val rocket = U1()
                rockets.add(rocket)
                if (rocket.canCarry(it)) {
                    rocket.carry(it)
                }
            }
        }

        return rockets
    }

    fun loadU2(items: ArrayList<Item>): ArrayList<Rocket> {
        val rockets = ArrayList<Rocket>()

        items.forEach {
            var stored = false
            for (rocket in rockets) {
                if(rocket.canCarry(it)) {
                    rocket.carry(it)
                    stored = true
                    break
                }
            }

            if (!stored) {
                val rocket = U2()
                rockets.add(rocket)
                if (rocket.canCarry(it)) {
                    rocket.carry(it)
                }
            }
        }

        return rockets
    }
}