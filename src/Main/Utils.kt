package main

object Utils {
    fun failOrSuccess(percentage: Int, cargoWeight: Double, limitWeight: Double): Boolean {
        val failChance = (percentage * (cargoWeight / limitWeight))
        val randomNumber = (1..100).random()
        return randomNumber > failChance
    }
}