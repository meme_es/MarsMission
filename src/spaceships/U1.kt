package spaceships

import main.Utils

class U1: Rocket(rocketWeight = 10.0, limitWeight = 18.0, cost = 100, launchingFailPercent = 5, landingFailPercent = 1) {

    override fun launch(): Boolean {
        return Utils.failOrSuccess(percentage = launchingFailPercent, cargoWeight, limitWeight)
    }

    override fun land(): Boolean {
        return Utils.failOrSuccess(percentage = landingFailPercent, cargoWeight, limitWeight)
    }
}