package spaceships

import main.Item

open class Rocket(
    protected val rocketWeight: Double,
    protected val limitWeight: Double,
    val cost: Int,
    protected val launchingFailPercent: Int,
    protected val landingFailPercent: Int
    ): SpaceShip {
    protected var cargoWeight = 0.0
    private val items = arrayListOf<Item>()

    override fun launch(): Boolean {
        return true
    }

    override fun land(): Boolean {
        return true
    }

    override fun canCarry(item: Item): Boolean {
        return item.weight * 0.001 + cargoWeight + rocketWeight  <= limitWeight
    }

    override fun carry(item: Item) {
        cargoWeight += (item.weight * 0.001)
        items.add(item)
    }
}