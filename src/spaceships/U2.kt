package spaceships

import main.Utils

class U2: Rocket(rocketWeight = 18.0, limitWeight = 29.0, cost = 120, launchingFailPercent = 4, landingFailPercent = 8) {

    override fun launch(): Boolean {
        return Utils.failOrSuccess(percentage = launchingFailPercent, cargoWeight, limitWeight)
    }

    override fun land(): Boolean {
        return Utils.failOrSuccess(percentage = landingFailPercent, cargoWeight, limitWeight)
    }
}