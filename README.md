
# Mars Mission

> This is a console project built using Kotlin and Intellij.

The project is about a simulation of rockets with a certain probability to explode during the launch or crash when they land according their cargo, the simulation implements two rockets U1 and U2 which the application gives input files with the weight of the cargo they can carry. The simulation is setting and running from the main function.

## Built With

- Kotlin
- Intellij

## Getting Started

Get a local copy running the following command.

`git clone https://gitlab.com/meme_es/MarsMission.git`

### Prerequisites

- Java JDK
- Intellij

### Usage

Open the project using and configuring your favorite editor pointing to the rigth Java JDK.

To run the project using Intellij open the Main.kt file located in ./src/main/, select the run button shaped like a triangle located in the left side of the main function, or up in the menu bar, or just pres shift+10.

## Authors

👤 **Meme**

- GitLab: [@meme_es](https://gitlab.com/meme_es)
- Linkedin: [linkedin](https://www.linkedin.com/in/manuel-elias/)

## 🤝 Contributing

Contributions, issues and feature requests are welcome!

Feel free to check the [issues page](https://gitlab.com/meme_es/MarsMission/-/issues).

## Show your support

Give a ⭐️ if you like this project!


## 📝 License

This project is unlicensed.
